"use strict";

const BASE_URL = "https://ajax.test-danit.com/api/json/";
const ENTITIES = ["users", "posts"];
class Request {
  constructor([userUrl, postUrl]) {
    this.urls = [userUrl, postUrl];
  }
  getRequest() {
    const arr = [];
    const urlArr = Array.from(this.urls);

    // симуляция 2х секундной подгрузки с сервера(можно убрать сеттаймаут, посты появятся сразу при открытии страницы)

    const timer = setTimeout(() => {
      urlArr.forEach(async (element, value) => {
        await fetch(element)
          .then((response) => response.json())
          .then((data) => {
            arr.push(data);
          });
        if (value === 1) {
          const card = new Card(arr);
          card.makeTweet();
        }
      });
    }, 2000);
  }
}

class Card {
  constructor(array) {
    this.array = array;
    this.fragment = document.createDocumentFragment();
  }
  makeTweet() {
    for (let key of this.array[1]) {
      const userPost = this.array[0].find((user) => {
        if (user.id === key.userId) {
          return user;
        }
      });
      this.render(key, userPost);
      if (key.id === 30) {
        document.querySelector(".posts_wrapper").append(this.fragment);
        break;
      }
    }
  }
  render(post, user) {
    const div = document.createElement("div");
    const titleText = document.createElement("p");
    const bodyText = document.createElement("p");
    const nameText = document.createElement("a");
    const mailText = document.createElement("a");

    div.id = post.id;
    div.userId = post.userId;

    titleText.textContent = post.title;
    titleText.classList.add("title_text");

    bodyText.textContent = post.body;
    bodyText.classList.add("body_text");

    nameText.textContent = user.name;
    nameText.classList.add("name_text");
    nameText.setAttribute("href", "#");

    mailText.textContent = user.email;
    mailText.setAttribute("href", "#");
    mailText.classList.add("mail_text");

    div.append(nameText, mailText, titleText, bodyText);
    div.classList.add("post_wrapper");
    this.addRemoveBtn(div);
    this.addChangeBtn(div);
    this.fragment.append(div);
  }
  addRemoveBtn(div) {
    const removeBtnWrap = document.createElement("div");
    removeBtnWrap.classList.add("remove_btn_wrapper");
    const removeBtn = document.createElement("button");
    removeBtn.classList.add("close_btn");
    removeBtn.addEventListener("click", this.listenRemove);
    removeBtnWrap.append(removeBtn);
    div.prepend(removeBtnWrap);
  }
  addChangeBtn(div) {
    const changeBtnWrap = document.createElement("div");
    changeBtnWrap.classList.add("change_btn_wrapper");
    const changeBtn = document.createElement("button");
    changeBtn.textContent = "Change..";
    changeBtn.classList.add("change_btn");
    changeBtn.addEventListener("click", this.listenChange);
    changeBtnWrap.append(changeBtn);
    div.append(changeBtnWrap);
  }
  listenRemove() {
    fetch(BASE_URL + "posts/" + this.parentNode.parentNode.id)
      .then((response) => {
        if (response.status === 200) {
          this.parentNode.parentNode.remove();
        }
      })
      .catch((e) => {
        alert(e);
      });
  }
  listenChange() {
    const currentPost = this.parentNode.parentNode;
    const currentText = currentPost.childNodes[4];
    if (this.className !== "change_btn changed") {
      currentText.setAttribute("contenteditable", "true");
      this.textContent = "Save";
      this.classList.toggle("changed");
    } else {
      this.classList.toggle("changed");
      this.textContent = "Change..";
      currentText.setAttribute("contenteditable", "false");

      fetch(BASE_URL + "posts/" + currentPost.id, {
        method: "PUT",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify({
          body: currentText.textContent,
          id: currentPost.id,
          title: currentPost.childNodes[3].textContent,
          userId: currentPost.userId,
        }),
      })
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
        });
    }
  }
}

const request = new Request([BASE_URL + ENTITIES[0], BASE_URL + ENTITIES[1]]);
request.getRequest();
